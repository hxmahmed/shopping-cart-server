var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
// var brands = require('./routes/brand');
// var category = require('./routes/category');
// var order_detail = require('./routes/order_detail');
// var order_status = require('./routes/order_status');
// var order = require('./routes/order');
// var payment_type = require('./routes/payment_type');
// var product_attrubite = require('./routes/product_attrubite');
// var product_type = require('./routes/product_type');
// var product = require('./routes/product');
// var shipping_details = require('./routes/shipping_details');
// var shipping_type = require('./routes/shipping_type');
// var sub_category = require('./routes/sub_category');
// var user_type = require('./routes/user_type');
// var users = require('./routes/users');
// var customers = require('./routes/customers');




var app = express();



var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'localhost:4200');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,token');

    next();
}
 var api = require('./modules/api/module');
  var authApi = require('./modules/api/authenticateModule');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(allowCrossDomain);

 app.use('/', index);
 app.use('/api', api);
 app.use('/auth-api', authApi);

// app.use('/brand', brands);
// app.use('/category', category);
// app.use('/customers', customers);
// app.use('/order_detail', order_detail);
// app.use('/order_status', order_status);
// app.use('/order', order);
// app.use('/payment_type', payment_type);
// app.use('/product_attrubite', product_attrubite);
// app.use('/product_type', product_type);
// app.use('/product', product);
// app.use('/shipping_details', shipping_details);
// app.use('/shipping_type', shipping_type);
// app.use('/sub_category', sub_category);
// app.use('/user_type', user_type);
// app.use('/users', users);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
