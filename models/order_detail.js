'use strict';
module.exports = function(sequelize, DataTypes) {
  var order_detail = sequelize.define('order_detail', {
    order_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    product_type_id: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    price: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        order_detail.belongsTo(models.product, {foreignKey: 'product_id',as: "product"});
        order_detail.belongsTo(models.product_type, {foreignKey: 'product_type_id',as: "product_type"});
        order_detail.belongsTo(models.Order, {foreignKey: 'order_id',as: "Order"});
      }
    }
  });
  return order_detail;
};