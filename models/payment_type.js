'use strict';
module.exports = function(sequelize, DataTypes) {
  var payment_type = sequelize.define('payment_type', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        payment_type.hasMany(models.Order, {as:"Order",foreignKey:"payment_type"});
      }
    }
  });
  return payment_type;
};