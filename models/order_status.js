'use strict';
module.exports = function(sequelize, DataTypes) {
  var order_status = sequelize.define('order_status', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        order_status.hasMany(models.Order, {as:"Orders",foreignKey:"status_id"});
      }
    }
  });
  return order_status;
};