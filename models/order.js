'use strict';
module.exports = function(sequelize, DataTypes) {
  var Order = sequelize.define('Order', {
    customer_id: DataTypes.INTEGER,
    date: DataTypes.DATE,
    payment_type: DataTypes.STRING,
    status_id: DataTypes.INTEGER,
    payment_type_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
           Order.belongsTo(models.Customers, {foreignKey: 'customer_id',as: "Customer"});
        Order.belongsTo(models.payment_type, {foreignKey: 'payment_type',as: "payment_types"});
        Order.belongsTo(models.order_status, {foreignKey: 'status_id',as: "order_statuses"});
  //      Order.belongsTo(models.Shipping_details, {foreignKey: 'order_id',as: "shipping_detail"});
  //      Order.hasOne(models.Shipping_details, {as: 'shipping_detail', foreignKey: 'order_id'});
   //     Order.hasOne(models.Shipping_details, {as: 'shipping_detail', foreignKey: 'order_id'});
        Order.hasMany(models.order_detail, {as:"Order_details",foreignKey:"order_id"});
  
      }
    }
  });
  return Order;
};