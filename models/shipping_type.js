'use strict';
module.exports = function(sequelize, DataTypes) {
  var Shipping_type = sequelize.define('Shipping_type', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
       Shipping_type.hasMany(models.Shipping_details, {as:"Shipping_details",foreignKey:"shipping_type_id"});
      }
    }
  });
  return Shipping_type;
};