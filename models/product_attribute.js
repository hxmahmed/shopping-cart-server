'use strict';
module.exports = function(sequelize, DataTypes) {
  var product_attribute = sequelize.define('product_attribute', {
    product_type_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    value: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
     product_attribute.belongsTo(models.product_type, {foreignKey: 'product_type_id',as: "product_type",onDelete: 'CASCADE',hooks: true,foreignKeyConstraint: true});
      }
    }
  });
  return product_attribute;
};