'use strict';
module.exports = function(sequelize, DataTypes) {
  var Category = sequelize.define('Category', {
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    descripton: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Category.hasMany(models.Sub_Category, {as:"Sub_categories",foreignKey:"category_id",onDelete:"RESTRICT"});
      }
    }
  });
  return Category;
};