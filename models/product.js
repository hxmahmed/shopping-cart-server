'use strict';
module.exports = function(sequelize, DataTypes) {
  var product = sequelize.define('product', {
    sub_category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    meta_title: DataTypes.STRING,
    meta_description: DataTypes.STRING,
    short_description: DataTypes.STRING,
    brand_id: DataTypes.INTEGER,
    multiple_images: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          product.belongsTo(models.Sub_Category, {foreignKey: 'sub_category_id',as: "Sub_category"});
          product.belongsTo(models.Brand, {foreignKey: 'brand_id',as: "Brand"});
         product.hasMany(models.product_type, {as:"product_types",foreignKey:"product_id"});
          product.hasMany(models.order_detail, {as:"order_detail",foreignKey:"product_id"});
      }
    }
  });
  return product;
};