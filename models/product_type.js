'use strict';
module.exports = function(sequelize, DataTypes) {
  var product_type = sequelize.define('product_type', {
    product_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    purchase_price: DataTypes.FLOAT,
    whole_sale_price: DataTypes.FLOAT,
    retail_price: DataTypes.FLOAT,
    special_price: DataTypes.FLOAT,
    discounted_price: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          product_type.belongsTo(models.product, {foreignKey: 'product_id',as: "product",onDelete: 'CASCADE',hooks: true,foreignKeyConstraint: true});
          product_type.hasMany(models.product_attribute, {as:"product_attributes",foreignKey:"product_type_id"});
         product_type.hasMany(models.order_detail, {as:"order_detail",foreignKey:"product_type_id"});
      }
    }
  });
  return product_type;
};