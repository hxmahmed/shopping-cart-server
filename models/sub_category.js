'use strict';
module.exports = function(sequelize, DataTypes) {
  var Sub_Category = sequelize.define('Sub_Category', {
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    sort_order: DataTypes.STRING,
    description: DataTypes.STRING,
    category_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
         Sub_Category.belongsTo(models.Category, {foreignKey: 'category_id',as: "Category"});
         Sub_Category.hasMany(models.product, {as:"Products",foreignKey:"sub_category_id"});
    
      }
    }
  });
  return Sub_Category;
};