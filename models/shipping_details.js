'use strict';
module.exports = function(sequelize, DataTypes) {
  var Shipping_details = sequelize.define('Shipping_details', {
    order_id: DataTypes.INTEGER,
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    address: DataTypes.STRING,
    shipping_type_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Shipping_details.belongsTo(models.Order, {foreignKey: 'order_id',as: "Order"});
        Shipping_details.belongsTo(models.Shipping_type, {foreignKey: 'shipping_type_id',as: "shipping_type"});
      }
    }
  });
  return Shipping_details;
};