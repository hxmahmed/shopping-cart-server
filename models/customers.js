'use strict';
module.exports = function(sequelize, DataTypes) {
  var Customers = sequelize.define('Customers', {
    Username: DataTypes.STRING,
    password: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    contact_no: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
       Customers.hasMany(models.Order, {as:"orders",foreignKey:"customer_id"});
      }
    }
  });
  return Customers;
};