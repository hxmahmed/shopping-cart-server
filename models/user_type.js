'use strict';
module.exports = function(sequelize, DataTypes) {
  var User_type = sequelize.define('User_type', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
         User_type.hasMany(models.Users, {as:"Users",foreignKey:"user_type_id"});
      }
    }
  });
  return User_type;
};