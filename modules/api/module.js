var express = require('express');
var jwt     = require('jsonwebtoken');
var router = express.Router();

//var secureRoutes =  express.Router();
//process.env.SECRET_KEY = "shoppingcartkey";


var brands = require('./controllers/brands');
var categories = require('./controllers/categories');
var order_detail = require('./controllers/order_detail');
var order_status = require('./controllers/order_status');
var order = require('./controllers/order');
var payment_type = require('./controllers/payment_type');
var product_attrubite = require('./controllers/product_attribute');
var product_type = require('./controllers/product_type');
var product = require('./controllers/product');
var shipping_details = require('./controllers/shipping_details');
var shipping_type = require('./controllers/shipping_type');
var Sub_Category = require('./controllers/sub_category');
var user_type = require('./controllers/user_type');
var users = require('./controllers/users');
var customers = require('./controllers/customers');
var shipping = require('./controllers/shipping_details');

var authenticateController =require('./controllers/authenticateController');
var customerauthenticateController =require('./controllers/customerAuthenticateController');


/* Registering All the routes */

///////////////////////////////////
//authenticate
router.post('/authenticate/' ,authenticateController.authenticate);
router.post('/customerAuthenticate/' ,customerauthenticateController.customerAuthenticate);
///////////////////////////////////

router.get('/brands/', brands.view);
router.get('/brands/searchById/:id', brands.searchById);

//-----------------------------------------------------------
 router.get('/category', categories.view);
 router.get('/category/searchById/:id', categories.searchById);
 router.post('/category/create', categories.create);
// //-----------------------------------------------------------
// app.use('/customers', customers);

 router.post('/customers/create', customers.create);
 
// //-----------------------------------------------------------
// app.use('/order_detail', order_detail);
//  router.get('/order_detail', order_detail.view);
//  router.post('/order_detail/create', order_detail.create);
// // router.post('/edit', brands.edit);
//  router.get('/order_detail/delete/:id', order_detail.delete);
//  router.get('/order_detail/searchById/:id', order_detail.searchById);
// //-----------------------------------------------------------
// app.use('/order_status', order_status);
// //-----------------------------------------------------------
// app.use('/order', order);
// //-----------------------------------------------------------
// app.use('/payment_type', payment_type);
// //-----------------------------------------------------------
// app.use('/product_attrubite', product_attrubite);
// //-----------------------------------------------------------
// app.use('/product_type', product_type);
// //-----------------------------------------------------------
// app.use('/product', product);
router.get('/sub_category', Sub_Category.view);
router.get('/sub_category/searchById/:id', Sub_Category.searchById);
router.get('/sub_category/searchByCategoryId/:id', Sub_Category.searchByCategoryId);
// //-----------------------------------------------------------
// app.use('/shipping_details', shipping_details);
// //-----------------------------------------------------------
// app.use('/shipping_type', shipping_type);
// //-----------------------------------------------------------

 router.get('/product', product.view);
 router.get('/product/searchById/:id', product.searchById);
 router.get('/product/searchByName/:id', product.searchByName);
 router.get('/product/searchBySubCategoryId/:id', product.searchBySubCategoryId);


// //-----------------------------------------------------------

 router.get('/order', order.view);
router.get('/order/searchById/:id', order.searchById);
router.get('/order/searchByCustomerIdonlycart/:id', order.searchByCustomerIdonlycart);
router.get('/order/searchByCustomerIdonlybasket/:id', order.searchByCustomerIdonlybasket);
router.post('/order/create', order.create);
router.get('/order/delete/:id', order.delete);
router.post('/order/update/:id', order.update);

// //-----------------------------------------------------------

 router.get('/orderStatus', order_status.view);

//===========================================================

 router.get('/shipping', shipping.view);


router.post('/shipping/create', shipping.create);



// //-----------------------------------------------------------





module.exports = router;