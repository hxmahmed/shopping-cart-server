 var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var shipping_details = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

shipping_details.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Shipping_details.findOrCreate({
    where:{
        order_id: post.order_id
    }, 
    defaults: {
    //properties you want on create
        country: post.country,
        city: post.city,
        address : post.address,
        shipping_type_id : post.shipping_type_id
    }
}).spread( function(user,created){
  if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, "Success"
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "Failure"
            ));
    }
})

 




}

shipping_details.view = function (req, res,next){

    model.Shipping_details.findAll().then(function(shipping){

        if(!shipping.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            shipping , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            shipping , "Success"
                            
            ));
        }
    
  });


}

shipping_details.searchById = function (req, res,next){
    var id = req.params.id;
    model.Shipping_details.find({
    where:{
        id: id
    }
  })
  .then(function(shipping){
      if(shipping == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            shipping
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            shipping
                            
                            ));
      }
    
  });
 


}

shipping_details.delete = function (req, res,next){
    var id = req.params.id;
    model.Shipping_details.destroy({
    where:{
        id: id
    }
  })
    .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } );
 


}


module.exports = shipping_details;