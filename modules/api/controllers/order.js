    var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var order = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}




 
order.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);
    var response = [];

model.Order.create({
    

    customer_id : post.customer_id,
   	date: post.date,
    payment_type: post.payment_type,
    status_id: post.status_id,
    payment_type_id: post.payment_type_id,

       
    
}).then( function(user){

    response.push(user);
    if(user){

        model.order_detail.create({
    order_id : user.id,
  product_id: post.order_detail.product_id,
   	product_type_id: post.order_detail.product_type_id,
    	quantity: post.order_detail.quantity,
    price: post.order_detail.price,}
).then( function(users){
    if(users)
    {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user , "Success"
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "Failure"
            ));
    }

})
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "Failure"
            ));
    }
})


}



order.update = function (req, res,next){
    var id = req.params.id;
    post = requestHelper.parseBody(req.body);

    var update = { 
         customer_id : post.customer_id,
   	date: post.date,
    payment_type: post.payment_type,
    status_id: post.status_id,
    payment_type_id: post.payment_type_id,
};

var update2 = {
             order_id : post.order_id,
  product_id: post.product_id,
   	product_type_id: post.product_type_id,
    	quantity: post.quantity,
    price: post.price 
}
    
    var filter = {
  where: {
    id: id
  }
};

    var filter2 = {
  where: {
    id: id
  },

};

model.order_detail.update(update2,filter2)  .then(function(record)
  {
        if(record==1){
            model.order_detail.find({where : {id :id }}).then(function(records){
                console.log(records.order_id);
            model.Order.update(update,{where: {id : records.order_id}})  .then(function(record)
  {
        if(record==1){
         res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.Response(
                            constants.MESSAGES.UPDATE.SUCCESS,
                            record , "Success"
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.UPDATE.ERROR,
                            record[0] , "Failure"
            ));
    }
  }

           
            
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );
        });
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.UPDATE.ERROR,
                            record , "Failure"
            ));
    }
  }
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );

}




order.view = function (req, res,next){


    model.Order.findAll( {
        include: [
            {
        model : model.order_detail,
        as : "Order_details"
        }
    
    ]
         } ).then(function(product){

        if(!product.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
            ));
        }
    
  });


}

order.searchById = function (req, res,next){
    var id = req.params.id;
    model.Order.find({
    where:{
        id: id
    },
   include: [
            {
        model : model.order_detail,
        as : "Order_details"
        }
    
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
                            ));
      }
    
  });
 


}


order.searchByCustomerIdonlycart = function (req, res,next){
    var id = req.params.id;
    model.Order.findAll({
    where:{
        customer_id : id,
        status_id : 1
    },
   include: [
            {
        model : model.order_detail,
        as : "Order_details",
        include : [
            {
            model : model.product,
            as : "product"
        },
        {
            model : model.product_type,
            as : "product_type"
        }
         
        ]
    }
    ,
       {
            model : model.order_status,
            as : "order_statuses"
        }
    
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
                            ));
      }
    
  });
 


}

order.searchByCustomerIdonlybasket = function (req, res,next){
    var id = req.params.id;
    model.Order.findAll({
    where:{
        customer_id : id,
        $or: [
        // {
        //     customer_id: 
        //     {
        //         $eq: id
        //     }
        // }, 
        {
           status_id: 
            {
                $eq: 2
            }
        }, 
        {
           status_id: 
            {
                $eq: 3
            }
        }, 
        {
           status_id: 
            {
                $eq: 4
            }
        }
    ]
    },
   include: [
            {
        model : model.order_detail,
        as : "Order_details",
        include : [
            {
            model : model.product,
            as : "product"
        },
        {
            model : model.product_type,
            as : "product_type"
        }
         
        ]
    }
    ,
       {
            model : model.order_status,
            as : "order_statuses"
        }
    
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
                            ));
      }
    
  });
 


}




order.delete = function (req, res,next){
    var id = req.params.id;

      model.Order.find({
    where:{
        id: id
    },
    include : [
        {
        model : model.order_detail,
        as : "Order_details"
        }
    ]
  })
  .then(function(order){


     
    model.order_detail.destroy(
        {
            where : {
                id : order.Order_details[0].id
            }
        }
    ).then(function(orders){

        if(orders)
        {
            model.Order.destroy({
                where: {
                    id : order.id
                }

            }).then(function(deleted)
            {
                if(deleted)
                {
                    res.statusCode = constants.HTTP.CODES.SUCCESS;
                    res.json({"Message" :"Deleted","Status":"200"}); 
                }
                else
                {
                    res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
                    res.json({"Message" :"Not Deleted","Status":"404"}); 
                }
             });
        }
        else
        {
             res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
                  res.json({"Message" :"Deleted","Status":"200"}); 
        }
    }
    
    );

  });
 

 


}


module.exports = order;