 var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var users = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

users.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Users.findOrCreate({
    where:{

          $or: [
        {
            Username: 
            {
                $eq: post.username
            }
        }, 
        {
            email: 
            {
                $eq: post.email
            }
        }, 
        {
            contact_no: 
            {
                $eq: post.contact_no
            }
        }
    ]
  
    
    }, 
    defaults: {
    //properties you want on create
        Username: post.username,
        email: post.email,
        contact_no: post.contact_no,
        password: post.password,
        first_name: post.first_name,
        last_name: post.last_name,
        user_type_id: post.user_type_id
    }
}).spread( function(user,created){
   if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, constants.HTTP.CODES.SUCCESS
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , constants.HTTP.CODES.BAD_REQUEST
            ));
    }
}).catch(function(User){ res.send("Server Error")}
    
  );

  




}

users.view = function (req, res,next){

    model.Users.findAll(
        {include: [
         {
         model : model.User_type,
         as : "UserType"
         }
         ]} 
    ).then(function(user){

           if(!user.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            user, "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            user , "Success"
                            
            ));
        }
    
  }).catch(function(User){ res.send("Server Error")}
    
  );


}

users.searchById = function (req, res,next){
    var id = req.params.id;
    model.Users.find({
    // where:{
    //     id: id
    // },
    // include: ['UserType']
     where: {id: id}, include: [
         {
         model : model.User_type,
         as : "UserType"
         }
         ] 
  })
  .then(function(User){
      if(User == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            User
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            User
                            
                            ));
      }
    
  }).catch(function(User){ res.send("Server Error")}
    
  );
 


}

users.searchByUsername = function (req, res,next){
    var id = req.params.id;
    model.Users.find({
    // where:{
    //     id: id
    // },
    // include: ['UserType']
     where: {Username: id}, include: [
         {
         model : model.User_type,
         as : "UserType"
         }
         ] 
  })
  .then(function(User){
      if(User == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            User
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            User
                            
                            ));
      }
    
  }).catch(function(User){ res.send("Server Error")}
    
  );
 


}

users.delete = function (req, res,next){
    var id = req.params.id;
    model.Users.destroy({
    where:{
        id: id
    }
  })
    .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } ).catch(function(User){ res.send("Server Error")}
    
  );
 


}




users.update = function (req, res,next){
    var id = req.params.id;
    post = requestHelper.parseBody(req.body);

model.Users.update(
  {     Username: post.username,
        email: post.email,
        contact_no: post.contact_no,
        password: post.password,
        first_name: post.first_name,
        last_name: post.last_name,
        user_type_id: post.user_type_id },
  { where: { id: id } }
)
  .then(function(record)
  {
        if(record==1){
         res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.SUCCESS,
                            record
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.ERROR,
                            record
            ));
    }
  }
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );


  




}

module.exports = users;