   var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var product = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

// product.create = function (req, res,next){

//     post = requestHelper.parseBody(req.body);

// model.product.findOrCreate({
    
//         where:{
//         name: post.name

//     }, 
//     defaults: {
//     //properties you want on create
//     name: post.name,
//     sub_category_id: post.sub_category_id,
//     description: post.description,
//     meta_title: post.meta_title,
//     meta_description: post.meta_description,
//     short_description: post.short_description,
//     brand_id: post.brand_id,
//     multiple_images:post.multiple_images,}
       
    
// }).spread( function(user,created){
//     if(created){

//         model.product_type.create({
//     product_id : user.id,
//     name: post.product_type.name2,
//     quantity: post.product_type.quantity,
//     purchase_price: post.product_type.purchase_price,
//     whole_sale_price: post.product_type.whole_sale_price,
//     retail_price:post.product_type.retail_price,
//     special_price: post.product_type.special_price,
//     discounted_price: post.product_type.discounted_price}
// ).then( function(user){
//     if(user){
// var arrays = post.product_attribute;
// var response=[];
// arrays.forEach(function(element) {
    


// model.product_attribute.create({

//     product_type_id: user.id,
//     name: element.name,
//     value: element.value
   
//      }
// ).then( function(user){
//     if(user){

// response.push(""+responseHelper.formatResponse(
//                             constants.HTTP.CODES.SUCCESS,
//                             user));


//     }
//     else
//     {
// res.statusCode = constants.HTTP.CODES.BAD_REQUEST
// res.json('not ok product attribute');
//     }
// })

// }, this);

// res.statusCode = constants.HTTP.CODES.SUCCESS;
//          res.send(response.toString());
//     }
//     else
//     {
// res.statusCode = constants.HTTP.CODES.BAD_REQUEST
// res.json('not ok product type');
//     }
// })
//     }
//     else
//     {
//         res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
//          res.send(responseHelper.formatResponse(
//                             constants.MESSAGES.INSERT.EXIST,
//                             user
//             ));
//     }
// })

 




// }


 
product.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);
    var response = [];

model.product.findOrCreate({
    
        where:{
        name: post.name

    }, 
    defaults: {
    //properties you want on create
    name: post.name,
    sub_category_id: post.sub_category_id,
    description: post.description,
    meta_title: post.meta_title,
    meta_description: post.meta_description,
    short_description: post.short_description,
    brand_id: post.brand_id,
    multiple_images:post.multiple_images,}
       
    
}).spread( function(user,created){

    response.push(user);
    if(created){

        model.product_type.create({
    product_id : user.id,
    name: post.product_type.name2,
    quantity: post.product_type.quantity,
    purchase_price: post.product_type.purchase_price,
    whole_sale_price: post.product_type.whole_sale_price,
    retail_price:post.product_type.retail_price,
    special_price: post.product_type.special_price,
    discounted_price: post.product_type.discounted_price}
).then( function(users){
    if(users){
var arrays = post.product_attribute;
var promises=[];

arrays.forEach(function(element) {
    


var newPromise=model.product_attribute.create({

    product_type_id: users.id,
    name: element.name,
    value: element.value
   
     });

     promises.push(newPromise);
}); return Promise.all(promises).then(function(user) {


response.push(users);
response.push(user);
res.json(response);
})


    }
    else
    {
res.statusCode = constants.HTTP.CODES.BAD_REQUEST
//res.json('not ok product type');
  res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "not ok product type"
            ));
    }
})
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "Failure"
            ));
    }
})

 




}





product.view = function (req, res,next){

    model.product.findAll( {
        include: [
            {
        model : model.Brand,
        as : "Brand"
        }
            ,  
         {
         model : model.product_type,
         as : "product_types",
          include: [
         {
          model : model.product_attribute,
            as : "product_attributes"
        }]
    }
    ]
         } ).then(function(product){

        if(!product.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product
                            
            ));
        }
    
  });


}

product.searchById = function (req, res,next){
    var id = req.params.id;
    model.product.find({
    where:{
        id: id
    },
    include : [
         {
            model: model.Brand,
            as : "Brand"
        },
        {
            model : model.product_type,
            as : "product_types",
            include : [
                {
                    model : model.product_attribute,
                    as : "product_attributes"
                }
            ]
        }
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
                            ));
      }
    
  });
 


}



product.searchBySubCategoryId = function (req, res,next){
    var id = req.params.id;
    model.product.findAll({
    where:{
        sub_category_id: id
    },
    include : [
        {
            model : model.product_type,
            as : "product_types",
            include : [
                {
                    model : model.product_attribute,
                    as : "product_attributes"
                }
            ]
        }
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product , "Success"
                            
                            ));
      }
    
  });
 


}


product.searchByName = function (req, res,next){
    var id = req.params.id;
    model.product.find({
    where:{
        name: id
    },
    include : [
        {
            model : model.product_type,
            as : "product_types",
            include : [
                {
                    model : model.product_attribute,
                    as : "product_attributes"
                }
            ]
        }
    ]
  })
  .then(function(product){
      if(product == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            product
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            product
                            
                            ));
      }
    
  });
 


}


product.delete = function (req, res,next){
    var id = req.params.id;

      model.product.find({
    where:{
        id: id
    },
    include : [
        {
            model : model.product_type,
            as : "product_types",
            include : [
                {
                    model : model.product_attribute,
                    as : "product_attributes"
                }
            ]
        }
    ]
  })
  .then(function(products){


     
    model.product_attribute.destroy(
        {
            where : {
                product_type_id : products.product_types[0].id
            }
        }
    ).then(function(product_attribute){

        if(product_attribute)
        {
            model.product_type.destroy({
                where: {
                    id : products.product_types[0].id
                }

            }).then(function(product_typ)
            {
                if(product_typ)
                {
                    model.product.destroy({
                        where: {
                            id: products.id
                        }
                    }).then(function(product_deleted)
                    {
                        if(product_deleted)
                        {
                            res.json("deleted");
                        }
                    });
                }
            });
        }
    }
    
    );

  });
 

 


}


module.exports = product;