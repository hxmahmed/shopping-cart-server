var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var brands = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

brands.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Brand.findOrCreate({
    where:{
        name: post.name
    }, 
    defaults: {
    //properties you want on create
        description: post.description,
        logo: post.logo,
    }
}).spread( function(user,created){
  if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, constants.HTTP.CODES.SUCCESS
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , constants.HTTP.CODES.BAD_REQUEST
            ));
    }
})

    //  model.Brand.create(
    // {
    //     name:post.name,
    //     description: post.description,
    //     logo: post.logo,
    
    // })
    // .then(function(Brand){
        
    //     res.status= 200;
    //     res.send("successful");
    // });




}

brands.view = function (req, res,next){

    model.Brand.findAll().then(function(brands){

        if(!brands.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            brands , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            brands , "Success"
                            
            ));
        }
    
  });


}

brands.searchById = function (req, res,next){
    var id = req.params.id;
    model.Brand.find({
    where:{
        id: id
    }
  })
  .then(function(Brand){
      if(Brand == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Brand
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Brand
                            
                            ));
      }
    
  });
 


}

brands.delete = function (req, res,next){
    var id = req.params.id;
    model.Brand.destroy({
    where:{
        id: id
    }
  })
    .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } );
 


}

brands.update = function (req, res,next){
    var id = req.params.id;
    post = requestHelper.parseBody(req.body);
console.log("enter update");
model.Brand.update(
  { 
      name: post.name,
          description: post.description,
        logo: post.logo },
  { where: { id: id } }
)
  .then(function(record)
  {
        if(record==1){
         res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.SUCCESS,
                            record
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.ERROR,
                            record
            ));
    }
  }
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );


  




}


module.exports = brands;