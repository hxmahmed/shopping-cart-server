  var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var Sub_Category = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

Sub_Category.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Sub_Category.findOrCreate({
    
        where:{
        name: post.name
    }, 
    defaults: {
    //properties you want on create
    image: post.image,
    sort_order: post.sort_order,
    description: post.description,
    category_id: post.category_id
    }
        
    
}).spread( function(user,created){
   if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, constants.HTTP.CODES.SUCCESS
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , constants.HTTP.CODES.BAD_REQUEST
            ));
    }
})

 




}

Sub_Category.view = function (req, res,next){

    model.Sub_Category.findAll(

        {include: [
         {
         model : model.Category,
         as : "Category"
        },
        {
            model : model.product,
            as : "Products"
        }
         ]} 


    ).then(function(Sub_Category){

          if(!Sub_Category.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Sub_Category , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Sub_Category, "Success"
                            
            ));
        }
    
  });


}

Sub_Category.searchById = function (req, res,next){
    var id = req.params.id;
    model.Sub_Category.find({
    where:{
        id: id
    },
      include: [
            {
        model : model.Category,
        as : "Category"
        }]
    

  })
  .then(function(Sub_Category){
      if(Sub_Category == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Sub_Category
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Sub_Category
                            
                            ));
      }
    
  });
 


}



Sub_Category.searchByCategoryId = function (req, res,next){
    var id = req.params.id;
    model.Sub_Category.findAll({
    where:{
        category_id: id
    },
    
  })
  .then(function(Sub_Category){
      if(Sub_Category == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Sub_Category , "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Sub_Category, "Success"
                            
                            ));
      }
    
  });
 


}

Sub_Category.delete = function (req, res,next){
    var id = req.params.id;
    model.Sub_Category.destroy({
    where:{
        id: id
    }
  })
   .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } );
 


}



Sub_Category.update = function (req, res,next){
    var id = req.params.id;
    post = requestHelper.parseBody(req.body);
console.log("enter update");
model.Sub_Category.update(
  { 
     
        name: post.name,
    image: post.image,
    sort_order: post.sort_order,
    description: post.description,
    category_id: post.category_id},
  { where: { id: id } }
)
  .then(function(record)
  {
        if(record==1){
         res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.SUCCESS,
                            record
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.ERROR,
                            record
            ));
    }
  }
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );


  




}




module.exports = Sub_Category;