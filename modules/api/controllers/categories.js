var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var categories = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

categories.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Category.findOrCreate({
    where:{
        name: post.name
    }, 
    defaults: {
    //properties you want on create
        image: post.image,
        descripton: post.descripton,
        
    }
}).spread( function(user,created){
    if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, constants.HTTP.CODES.SUCCESS
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , constants.HTTP.CODES.BAD_REQUEST
            ));
    }
})

 




}

categories.view = function (req, res,next){

    model.Category.findAll({
        
      include : [
        {
            model : model.Sub_Category,
            as : "Sub_categories"
        }
    ]
    }).then(function(categories){

        if(!categories.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            categories , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            categories , "Success"
                            
            ));
        }
    
  });


}

categories.searchById = function (req, res,next){
    var id = req.params.id;
    model.Category.find({
    where:{
        id: id
    },
      include : [
        {
            model : model.Sub_Category,
            as : "Sub_categories"
        }
    ]
  })
  .then(function(Category){
      if(Category == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Category, "Failure"
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Category , "Success"
                            
                            ));
      }
    
  });
 


}

categories.delete = function (req, res,next){
    var id = req.params.id;
    model.Category.destroy({
    where:{
        id: id
    }
  })
  .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } );
 


}


categories.update = function (req, res,next){
    var id = req.params.id;
    post = requestHelper.parseBody(req.body);
console.log("enter update");
model.Category.update(
  { 
     
        name: post.name,
        image: post.image,
        descripton: post.descripton,},
  { where: { id: id } }
)
  .then(function(record)
  {
        if(record==1){
         res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.SUCCESS,
                            record
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.UPDATE.ERROR,
                            record
            ));
    }
  }
    
  )
  .catch(function(User){ res.send("Server Error")}
    
  );


  




}


module.exports = categories;