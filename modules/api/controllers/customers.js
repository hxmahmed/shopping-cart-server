 var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");
var model = require('../../../models');

var customers = {
  title: "Hello World",
  statusCode: constants.HTTP.CODES.SUCCESS
}

customers.create = function (req, res,next){

    post = requestHelper.parseBody(req.body);

model.Customers.findOrCreate({
    where:{
        $or: [
        {
            Username: 
            {
                $eq: post.username
            }
        }, 
        {
            email: 
            {
                $eq: post.email
            }
        }, 
        {
            contact_no: 
            {
                $eq: post.contactno
            }
        }
    ]
    }, 
    defaults: {
    //properties you want on create
    Username: post.username,
    password: post.password,
    first_name: post.firstname,
    last_name: post.lastname,
    email: post.email,
    contact_no: post.contactno,
    city: post.city,
    country: post.country,
    address: post.address
        
    }
}).spread( function(user,created){
    if(created){
         res.statusCode = constants.HTTP.CODES.SUCCESS;

        res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.SUCCESS,
                            user, "Success"
            ));
    }
    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
     // res.status = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.INSERT.EXIST,
                            user , "Failure"
            ));
    }
})

 




}

customers.view = function (req, res,next){

    model.Customers.findAll().then(function(customers){

             if(!customers.length)
        {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.NO_DATA,
                            customers , "Failure"
                            
            ));
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.Response(
                            constants.MESSAGES.VIEW.SUCCESS,
                            customers , "Success"
                            
            ));
        }
    
  });


}

customers.searchById = function (req, res,next){
    var id = req.params.id;
    model.Customers.find({
    where:{
        id: id
    }
  })
  .then(function(Customers){
      if(Customers == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Customers
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Customers
                            
                            ));
      }
    
  });
 


}


customers.searchByUsername = function (req, res,next){
    var id = req.params.id;
    model.Customers.find({
    where:{
        Username: id
    }
  })
  .then(function(Customers){
      if(Customers == null)
      {
  res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.NO_DATA,
                            Customers
                            
            ));
      }
      else
      {
 res.statusCode = constants.HTTP.CODES.SUCCESS;
         res.send(responseHelper.formatResponse(
                            constants.MESSAGES.VIEW.SUCCESS,
                            Customers
                            
                            ));
      }
    
  });
 


}

customers.delete = function (req, res,next){
    var id = req.params.id;
    model.Customers.destroy({
    where:{
        id: id
    }
  })
   .then(function(option)
  {
if(option== true)
{
return res.json({"Message" :"Deleted","Status":"200"}); 
}
else
{
return res.json({"Message" :"Not Deleted","Status":"200"}); 
}


  } );
 


}


module.exports = customers;