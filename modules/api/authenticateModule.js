var express = require('express');
var jwt     = require('jsonwebtoken');

var secureRoutes =  express.Router();

var brands = require('./controllers/brands');
var categories = require('./controllers/categories');
var order_detail = require('./controllers/order_detail');
var order_status = require('./controllers/order_status');
var order = require('./controllers/order');
var payment_type = require('./controllers/payment_type');
var product_attrubite = require('./controllers/product_attribute');
var product_type = require('./controllers/product_type');
var product = require('./controllers/product');
var shipping_details = require('./controllers/shipping_details');
var shipping_type = require('./controllers/shipping_type');
var Sub_Category = require('./controllers/sub_category');
var user_type = require('./controllers/user_type');
var users = require('./controllers/users');
var customers = require('./controllers/customers');
var shipping = require('./controllers/shipping_details');


secureRoutes.use(function(req,res,next) {

  //  var token = req.body.token || req.headers['token'];
  var token = req.headers['token'];

  console.log(token);

try{
    if(token != "client")
    {
       
        jwt.verify(token,process.env.SECRET_KEY, function(err,decode){
            if(err)
            {
                console.log("in");
            return res.json({"Message" :"invalid token","Status":"403"});    
            
         
            }
            else
            {
               console.log("in next");
                next();
            }
        })
      
    }
    else
    {
        if(token== "client")
        {
            res.json('UnAuthorised Access');
        }
        else{
 res.json('please give token');
        }
       
    }
}
catch(err)
{
    console.log(err);
}
    
})

process.env.SECRET_KEY = "shoppingcartkey";




secureRoutes.get('/users', users.view);
secureRoutes.post('/users/create', users.create);
secureRoutes.post('/users/update/:id', users.update);
secureRoutes.get('/users/delete/:id', users.delete);
secureRoutes.get('/users/searchById/:id', users.searchById);
secureRoutes.get('/users/searchByUsername/:id', users.searchByUsername);


secureRoutes.post('/product/create', product.create);
secureRoutes.get('/product/delete/:id', product.delete);


secureRoutes.post('/sub_category/create', Sub_Category.create);
secureRoutes.post('/sub_category/update/:id', Sub_Category.update);
secureRoutes.get('/sub_category/delete/:id', Sub_Category.delete);

secureRoutes.get('/customers', customers.view);
secureRoutes.get('/customers/delete/:id', customers.delete);
secureRoutes.get('/customers/searchById/:id', customers.searchById);
secureRoutes.get('/customers/searchByUsername/:id', customers.searchByUsername);


secureRoutes.post('/category/create', categories.create);
secureRoutes.post('/category/update/:id', categories.update);
secureRoutes.get('/category/delete/:id', categories.delete);


secureRoutes.post('/brands/create', brands.create);
secureRoutes.post('/brands/update/:id', brands.update);
secureRoutes.get('/brands/delete/:id', brands.delete);

secureRoutes.get('/shipping/searchById/:id', shipping.searchById);
secureRoutes.get('/shipping/delete/:id', shipping.delete);


// secureRoutes.get('/authenticate/' ,authenticateController.authenticate);


module.exports = secureRoutes;